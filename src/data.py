import pickle
import re
import sys
import argparse
import json
import gzip
import functools
import random
import logging
from frozendict import frozendict
import numpy
import scipy.sparse
from sklearn.metrics import f1_score, accuracy_score
from scipy.sparse.csgraph import connected_components


def norm(value, minval, maxval):
    return (value - minval) / (maxval - minval)


def inverse_norm(value, minval, maxval):
    return value * (maxval - minval) + minval


class Dataset(object):

    def collapse(self, entities, edges, max_collapse):
        if max_collapse <= 0:
            edge_matrices = {}
            for source_type, targets in edges.items():
                edge_matrices[source_type] = edge_matrices.get(source_type, {})
                for target_type, pairs in targets.items():
                    num = len(entities)
                    rows = [a for a, _ in pairs]
                    cols = [b for _, b in pairs]
                    vals = [True for _ in pairs]
                    edge_matrices[source_type][target_type] = scipy.sparse.coo_matrix((vals, (rows, cols)), shape=(num, num), dtype=numpy.bool).tocsr()
            #edge_matrices = {sf : {tf : m for tf, m in ts.items() if m.sum() > 0} for sf, ts in edge_matrices.items()}
            edge_matrices = {sf : {tf : m for tf, m in ts.items()} for sf, ts in edge_matrices.items()}            
            return (entities, edge_matrices)            
        else:
            duplicates = {}
            for i, e in enumerate(entities):
                e = frozendict(e)
                duplicates[e] = duplicates.get(e, [])
                duplicates[e].append(i)            
            to_merge = [ids for ids in duplicates.values() if len(ids) <= max_collapse]
            other = sum([ids for ids in duplicates.values() if len(ids) > max_collapse], [])
            equiv_ids = to_merge + [[i] for i in other]
            old_to_new = dict(sum([[(old, new) for old in olds] for new, olds in enumerate(equiv_ids)], []))
            edge_matrices = {}
            for source_type, dests in edges.items():
                edge_matrices[source_type] = {}
                for dest_type, old in dests.items():
                    vv, rr, cc = [], [], []
                    for old_source, old_target in old:
                        new_source = old_to_new[old_source]
                        new_target = old_to_new[old_target]
                        vv.append(True)
                        rr.append(new_source)
                        cc.append(new_target)
                    edge_matrices[source_type][dest_type] = scipy.sparse.csr_matrix((vv, (rr, cc)), shape=(len(equiv_ids), len(equiv_ids)), dtype=numpy.bool)
            new_entities = [entities[x[0]] for x in equiv_ids]
            return (new_entities, edge_matrices)
    
    def __len__(self):
        return len(self._values["*entitytype"]) if isinstance(self._values["*entitytype"], list) else self._values["*entitytype"].shape[0]

    @property
    def entity_types(self):
        return self._fields["*entitytype"][1].keys()

    def remove_entity_types(self, entity_types):
        keep = set()
        old_to_new = {}
        eids = [self._fields["*entitytype"][1][0][e] for e in entity_types]        
        for i, v in enumerate(numpy.array(self._values["*entitytype"].todense())):
            if v[0] not in eids:
                keep.add(i)
                old_to_new[i] = len(old_to_new)
        
        self._components = [c for c in [[old_to_new[i] for i in comp if i in keep] for comp in self._components] if len(c) > 0]
        keep = list(keep)
        
        keep_fields = set()
        for entity_type in list(self._entity_fields.keys()):
            if entity_type in entity_types:
                del self._entity_fields[entity_type]
            else:
                for field_name in self._entity_fields[entity_type]:
                    keep_fields.add(field_name)
        for field_name in list(self._fields.keys()):
            if field_name in keep_fields:
                self._values[field_name] = self._values[field_name][keep]
                self._masks[field_name] = self._masks[field_name][keep]
            else:
                del self._fields[field_name]
                
        for source_type in list(self._edges.keys()):
            if source_type in entity_types:
                del self._edges[source_type]
            else:
                for dest_type in list(self._edges[source_type].keys()):
                    if dest_type in entity_types:
                        del self._edges[source_type][dest_type]
                    else:
                        self._edges[source_type][dest_type] = self._edges[source_type][dest_type][keep, :][:, keep]
    
    @property
    def field_names(self):
        return self._fields.keys()

    @property
    def num_fields(self):
        return len(self._fields)

    @property
    def num_components(self):
        return len(self._components)

    @property
    def num_batches(self):
        return len(self._batches)
    
    def __str__(self):
        field_types = [t for t, _ in self._fields.values()]
        ncat = field_types.count("categorical")
        nnum = field_types.count("numeric")
        nseq = field_types.count("sequence")
        return "Dataset({} entities, {} fields ({}/{}/{} numeric/categorical/sequence), {} components)".format(len(self), self.num_fields, nnum, ncat, nseq, self.num_components)
    
    def consistent(self):
        """
        Verify that all fields and matrices correspond to the right number of entities
        """
        expected = self._values["*entitytype"].shape[0]
        for field_name, field_values in self._values.items():
            assert field_values.shape[0] == expected, "Field {} has shape {}".format(field_name, field_values.shape)
        for source_type, targets in self._edges.items():
            for target_type, matrix in targets.items():
                assert matrix.shape[0] == matrix.shape[1] == expected, "Edge matrix from {} to {} has shape {}".format(source_type, target_type, matrix.shape)
                #print(source_type, target_type, matrix.sum())
                #print(matrix)
                #assert matrix.sum() > 0, "Edge matrix from {} to {} sums to {}".format(source_type, target_type, matrix.sum())
        
    def __init__(self, ifd, line_count, max_categorical, max_collapse):
        """
        Two entities from the same record are, by definition, linked.  In addition, a
        set of entities of the same type with identical field values are collapsed
        IF the set is smaller than max_collapse.
        """
        entries, field_values = read_entries(ifd, line_count)
        self._fields, self._entity_fields = build_spec(field_values, max_categorical)
        self._values = {}
        self._components = []
        self._masks = {}
        self._batches = []
        for field_name in self._fields.keys():
            self._values[field_name] = []
            self._masks[field_name] = []
        entities, edges = unpack_entries(entries, self._fields)
        
        entities, self._edges = self.collapse(entities, edges, max_collapse)
        for entity in entities:
            encoded_entity, mask = self.encode(entity)
            entity_type = entity["*entitytype"]
            for field_name, (field_type, spec) in self._fields.items():
                self._values[field_name].append(encoded_entity.get(field_name, None))
                self._masks[field_name].append(mask[field_name])
        self._entity_fields = {k : list(sorted(v)) for k, v in self._entity_fields.items()}
        self._values = sparsify(self._values, self._fields)        
        self._masks = {field_name : numpy.array(values) for field_name, values in self._masks.items()}
        full_adjacency = sum(sum([list(x.values()) for x in self._edges.values()], [])) + scipy.sparse.eye(len(self))
        num, ids = connected_components(full_adjacency)
        components = {}    
        for i, c in enumerate(ids):
            components[c] = components.get(c, [])
            components[c].append(i)
            largest_component_size = max([len(x) for x in components.values()])
        for component in components.values():
            self._components.append(component)
        
    def __getitem__(self, index):
        entity = {}
        mask = {}
        for field_name, (field_type, spec) in self._fields.items():            
            value = self._values[field_name][index]
            mask[field_name] = self._masks[field_name][index]
            if field_type == "numeric":
                minval, maxval, mean, maxlength = spec
                entity[field_name] = value
            elif field_type == "categorical":
                item_to_id, rlu, maxlength = spec
                entity[field_name] = value
            elif field_type == "sequence":
                item_to_id, rlu, maxlength = spec
                entity[field_name] = value
        return (entity, mask)

    def component(self, index):
        indices = self._components[index]
        entities = {}
        masks = {}
        adjacencies = {}
        for field_name in self._fields.keys():
            entities[field_name] = self._values[field_name][indices]
            masks[field_name] = self._masks[field_name][indices]
        for source_type, targets in self._edges.items():
            adjacencies[source_type] = {}
            for target_type, matrix in targets.items():
                adjacencies[source_type][target_type] = matrix[indices, indices]
        return (entities, masks, adjacencies)

    def batch(self, indices):
        #indices = self._batches[index]
        entities = {}
        masks = {}
        adjacencies = {}
        for field_name in self._fields.keys():
            if self._fields[field_name][0] == "numeric":
                entities[field_name] = self._values[field_name][indices].todense().astype(numpy.float32)
                entities[field_name] = entities[field_name].astype(numpy.float32)
                
            elif self._fields[field_name][0] == "sequence":
                entities[field_name] = self._values[field_name][indices].todense()
            else:
                entities[field_name] = self._values[field_name][indices].todense()
            masks[field_name] = self._masks[field_name][indices]
        for source_type, targets in self._edges.items():
            adjacencies[source_type] = {}
            for target_type, matrix in targets.items():
                adjacencies[source_type][target_type] = matrix[indices, :][:, indices].todense().getA()
        return (entities, masks, adjacencies)
    
    def batchify(self, component_indices, max_size):
        batches = []
        #self._batches = []
        components = [self._components[ci] for ci in component_indices]
        random.shuffle(components)
        current = []
        while len(components) > 0:
            needed = max_size - len(current)
            if needed == 0:
                batches.append(current)
                current = []
            if len(components[0]) > needed:
                random.shuffle(components[0])
                current += components[0][0:needed]
                components[0] = components[0][needed:]
            else:
                current += components[0]
                components = components[1:]
        if len(current) > 0:
            batches.append(current)
        logging.info("Generated %d batches of size %d", len(batches), max_size)
        return batches
        
    def decode(self, entities, mask):
        retvals = []
        entities = {k : numpy.array(v.todense()) for k, v in entities.items()}
        for row in range(entities["*entitytype"].shape[0]):
            retval = {}
            for field_name, (field_type, spec) in self._fields.items():
                value = entities[field_name][row]
                if mask[field_name][row] == True:
                    if field_type == "numeric":
                        minval, maxval, mean, _ = spec
                        retval[field_name] = inverse_norm(value[0], minval, maxval)
                    elif field_type == "categorical":
                        _, id_to_item, _ = spec
                        retval[field_name] = id_to_item[value[0]]
                    elif field_type == "sequence":
                        item_to_id, id_to_item, maxlength = spec
                        retval[field_name] = "".join([id_to_item[v] for v in value if v != 0])
            retvals.append(retval)
        return retvals
    
    def encode(self, entity):
        retval = {}
        mask = {}
        for field_name, (field_type, spec) in self._fields.items():
            value = entity.get(field_name, None)
            mask[field_name] = field_name in entity
            entity_type = entity["*entitytype"]
            if field_type == "numeric":
                minval, maxval, mean, _ = spec
                nval = [norm(value, minval, maxval)] if mask[field_name] else [norm(mean, minval, maxval)]
            elif field_type == "categorical":
                item_to_id, _, _ = spec
                nval = [item_to_id[value]] if mask[field_name] else [0]
            elif field_type == "sequence":
                item_to_id, _, maxlength = spec
                nval = [item_to_id[v] for v in value] if mask[field_name] else []
            retval[field_name] = nval if field_name in self._entity_fields[entity_type] else []
        return (retval, mask)

    
def sparsify(values, fields):
    retval = {}
    for field_name, vals in values.items():
        nrows = len(vals)
        ncols = fields[field_name][1][-1]
        items = []
        vv = []
        rr = []
        cc = []
        for row, vs in enumerate(vals):
            for col, v in enumerate(vs):
                vv.append(v)
                rr.append(row)
                cc.append(col)                
        retval[field_name] = scipy.sparse.csr_matrix((vv, (rr, cc)), shape=(nrows, ncols))
    return retval

    
def read_entries(ifd, line_count=None):
    entries = []
    field_values = {"*entitytype" : set()}
    for line_num, line in enumerate(ifd):
        entry = {}
        if line_count != None and line_num >= line_count:
            break
        j = json.loads(line)
        for field_name, value in j.items():
            entity_type = field_name.split("_")[0]
            entry[entity_type] = entry.get(entity_type, {})                
            entry[entity_type][field_name] = value
            field_values["*entitytype"].add(entity_type)
            field_values[field_name] = field_values.get(field_name, set())
            field_values[field_name].add(value)            
        entries.append(entry)
    return (entries, field_values)
    

def unpack_entries(entries, spec):
    entities = []
    edges = {}
    count = 0
    for entry in entries:
        entity_ids = {}
        for entity_type, fields in entry.items():
            entity_id = len(entities)
            fields["*entitytype"] = entity_type
            entities.append(fields)            
            entity_ids[entity_type] = entity_ids.get(entity_type, [])
            entity_ids[entity_type].append(entity_id)
        for type_a, a_ids in entity_ids.items():
            edges[type_a] = edges.get(type_a, {})
            for type_b, b_ids in [x for x in entity_ids.items()]:
                edges[type_a][type_b] = edges[type_a].get(type_b, set())
                for a_id in a_ids:
                    for b_id in b_ids:
                        if a_id != b_id:
                            edges[type_a][type_b].add((a_id, b_id))
    return (entities, edges)


def build_spec(field_values, max_categorical=10):
    spec = {}
    entity_fields = {}
    for field_name, vals in field_values.items():
        if "_" in field_name:
            entity = field_name.split("_")[0]
            entity_fields[entity] = entity_fields.get(entity, set(["*entitytype"]))
            entity_fields[entity].add(field_name)
        
        if all([isinstance(val, (int, float)) for val in vals]):
            vals = list(sorted(vals))
            mean = sum(vals) / len(vals)
            spec[field_name] = ("numeric", (vals[0], vals[-1], mean, 1))            
        elif len(vals) <= max_categorical or field_name == "*entitytype":
            lookup = {"<UNK>" : 0}
            for val in vals:
                lookup[val] = lookup.get(val, len(lookup))
            rlookup = {v : k for k, v in lookup.items()}
            spec[field_name] = ("categorical", (lookup, rlookup, 1))
        else:
            lookup = {"<UNK>" : 0}
            lists = all([isinstance(val, list) for val in vals])
            max_length = sorted([len(val) for val in vals])[-1]
            for val in vals:
                for c in (val if lists else str(val)):
                    lookup[c] = lookup.get(c, len(lookup))
            rlookup = {v : k for k, v in lookup.items()}
            spec[field_name] = ("sequence", (lookup, rlookup, max_length))

    return (spec, entity_fields)


def get_components(num, edges):
    rows = []
    cols = []
    vals = []
    for a, b in edges:
        rows.append(a)
        cols.append(b)
        vals.append(True)
        rows.append(b)
        cols.append(a)
        vals.append(True)        
    adjacency = scipy.sparse.coo_matrix((vals, (rows, cols)), shape=(num, num), dtype=numpy.bool)
    num, ids = connected_components(adjacency)
    components = {}    
    for i, c in enumerate(ids):
        components[c] = components.get(c, [])
        components[c].append(i)
    largest_component_size = max([len(x) for x in components.values()])
    logging.info("Found %d connected components with maximum size %d", len(components), largest_component_size)
    return components
