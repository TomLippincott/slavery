import argparse
import gzip
import json
import random

if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument("-o", "--output", dest="output", help="Output file")
    parser.add_argument("-c", "--count", dest="count", type=int, default=1000, help="Number of entries to generate")
    args = parser.parse_args()
    
    with gzip.open(args.output, "wt") as ofd:
        for i in range(args.count):
            a_cat = random.randint(0, 10)
            b_cat = random.randint(0, 10)
            fields = {}

            ra = random.randint(0, 20)
            rb = random.randint(0, 20)
            
            fields["a_label"] = "a_{}".format(a_cat)
            a_c1, a_c2 = ("4", "3") if b_cat > 5 else ("2", "1")
            fields["a_categorical"] = "b_{}".format(b_cat)
            fields["a_numeric"] = b_cat
            fields["a_sequential"] = "".join([a_c1] * ra + [a_c2] * rb)
            
            fields["b_label"] = "b_{}".format(b_cat)
            b_c1, b_c2 = ("1", "2") if a_cat > 5 else ("3", "4")
            fields["b_categorical"] = "a_{}".format(a_cat)
            fields["b_numeric"] = a_cat
            fields["b_sequential"] = "".join([b_c1] * ra + [b_c2] * rb)
            
            ofd.write(json.dumps(fields) + "\n")

