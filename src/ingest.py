import argparse
import logging
import pandas
from elasticsearch import Elasticsearch
from elasticsearch.helpers import bulk
import os.path
import json
import gzip
import re
import datetime
import calendar

months = [x.lower() for x in list(calendar.month_name)]

if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    
    parser.add_argument("-i", "--input", dest="input", action="append", nargs=2, help="Excel inputs of form -i FILE_NAME SHEET_NAME")
    parser.add_argument("-o", "--output", dest="output")
    parser.add_argument("-H", "--host", dest="host", default="localhost", help="Host")
    parser.add_argument("-p", "--port", dest="port", type=int, default=9200, help="Port")
    parser.add_argument("-I", "--index_name", dest="index_name", default="original_slavery", help="Index name")
    parser.add_argument(dest="inputs", nargs="+")
    args = parser.parse_args()

    logging.basicConfig(level=logging.INFO)

    # inputs = {}
    # for fname, sname in args.input:
    #     inputs[fname] = inputs.get(fname, []) + [sname]

    fields = set()
    actions = []
    #for fname, snames in inputs.items():
    for fname in args.inputs:
        logging.info("Reading file '%s'", fname)
        sheets = pandas.read_excel(fname, sheet_name=None)
        for sname in [s for s in sheets.keys() if not s.startswith("DPCache")]:
            logging.info("Processing sheet '%s'", sname)
            sheet = sheets[sname]
            for field in sheet.iloc[0].to_dict().keys():
                if not field.startswith("Unnamed"):
                    fields.add(field.replace(".", "_"))
            for i in range(len(sheet)):
                item = {k.replace(".", "_") : str(v) for k, v in sheet.iloc[i].to_dict().items() if not k.startswith("Unnamed")}
                item["source_file"] = os.path.basename(fname)
                item["source_sheet"] = sname
                item["source_row"] = i + 1
                actions.append({"_source" : item, "_index" : args.index_name})
    properties = {f : {"type" : "text"} for f in fields}
    properties["source_file"] = {"type" : "keyword"}
    properties["source_sheet"] = {"type" : "keyword"}
    properties["source_row"] = {"type" : "integer"}

    dates = ["notice_event", "notice", "voyage_arrival", "voyage_departure", "voyage_manifest"]
    numeric = ["slave_age", "vessel_tonnage", "notice_reward_amount", "voyage_count", "notice_party_size", "owner_count"]
    coll_locs = ["owner_location", "owner_state", "owner_county", "owner_city", "owner_country"]
    remove = ["voyage_port_2"]
    
    if args.output:
        with gzip.open(args.output, "wt") as ofd:
            for a in actions:
                src = a["_source"]
                for k in list(src.keys()):
                    v = str(src[k])
                    if re.match("^\s*not", v) or re.match("^\s*\?\s*$", v) or v == "nan":
                        del src[k]
                    else:
                        src[k] = v.replace("?", "").strip()
                for nf in numeric:
                    if nf in src:
                        try:
                            src[nf] = float(src[nf])
                        except:
                            del src[nf]
                        
                if any([l in src for l in coll_locs]):
                    vals = []
                    for k in coll_locs:
                        if k in src:
                            vals.append(src[k])
                            del src[k]
                    src["owner_location"] = " ".join(vals)
                for d in dates:
                    df = "{}_day".format(d)
                    ddf = "{}_date".format(d)
                    mf = "{}_month".format(d)
                    yf = "{}_year".format(d)
                    if ddf in src:
                        toks = src[ddf].split()
                        #del src[ddf]
                        if len(toks) == 3:
                            day, month, year = toks
                            day = int(day)
                            if day > 31:
                                day = 28
                            month = src[mf]
                            year = int(float(src[yf]))
                            #try:
                            src[ddf] = datetime.date(year, months.index(month.lower()), day).toordinal()
                        elif len(toks) == 2:
                            month, year = toks
                            day = 1
                            try:
                                year = int(float(year))
                                src[ddf] = datetime.date(year, months.index(month.lower()), day).toordinal()
                            except:
                                if ddf in src:
                                    del src[ddf]                              
                                #except:
                            #    print(src)

                    elif all([x in src for x in [df, mf, yf]]):
                        try:
                            day = int(float(src[df]))
                            month = src[mf]
                            year = int(float(src[yf]))
                            date = "{} {} {}".format(year, month, day)
                            src[ddf] = datetime.date(year, months.index(month.lower()), day).toordinal()
                            #src[ddf] = src.get(ddf, date)
                        except:
                            if ddf in src:
                                del src[ddf]
                    elif ddf in src:
                        del src[ddf]    
                    for f in [df, mf, yf] + remove:
                        if f in src:
                            del src[f]
                    
                for pt in ["author", "slave", "owner", "shipper", "captain", "consignor"]:
                    s = "{}_sex".format(pt)
                    if s in src:                        
                        src[s] = src[s].lower()
                        if src[s] not in ["m", "f"]:
                            del src[s]
                            #print(src[s])
                            #print(src)
                        
                    fn = "{}_first_name".format(pt)
                    ln = "{}_last_name".format(pt)
                    if ln in src or fn in src:
                        parts = ([src[fn]] if fn in src else []) + ([src[ln]] if ln in src else [])
                        src["{}_name".format(pt)] = " ".join(parts)
                        if ln in src:
                            del src[ln]
                        if fn in src:
                            del src[fn]
                            
                    oc = "{}_owner_count".format(pt)
                    if oc in src:
                        try:
                            src[oc] = float(src[oc])
                        except:
                            del src[oc]
                            
                    hf = "{}_height_feet".format(pt)
                    hi = "{}_height_inches".format(pt)
                    if hf in src:

                        feet = float(src[hf])
                        if hi in src:
                            inches = float(src[hi])
                        else:
                            inches = 0.0
                        src["{}_height".format(pt)] = feet + (inches / 12.0)
                        del src[hf]
                    if hi in src:
                        del src[hi]
                src["source_row"] = int(src["source_row"])
                ofd.write(json.dumps(src) + "\n")
    else:
        es = Elasticsearch([{"host" : args.host, "port" :  args.port}])
        if es.indices.exists(args.index_name):
            es.indices.delete(index=args.index_name)            

        es.indices.create(index=args.index_name, body={"mappings" : {"properties" : properties}})
        es.indices.put_settings(index=args.index_name, body={"index" : { "max_result_window" : 500000 }})

        bulk(index=args.index_name, actions=actions, raise_on_error=True, client=es)
