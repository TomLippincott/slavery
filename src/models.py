import pickle
import re
import sys
import argparse
from frozendict import frozendict
import torch
import json
import numpy
import scipy.sparse
import gzip
from torch.utils.data import DataLoader, Dataset
import functools
import numpy
import random
import logging
from torch.optim import Adam, SGD
from torch.optim.lr_scheduler import ReduceLROnPlateau
from torch.nn.utils.rnn import pack_padded_sequence, pad_packed_sequence
import torch.nn.functional as F


# (batch_count x (entity_representation_size + (bottleneck_size * relation_count)) :: Float) -> (batch_count x entity_representation_size :: Float)
class Autoencoder(torch.nn.Module):
    def __init__(self, sizes):
        super(Autoencoder, self).__init__()
        self._encoding_layers = []
        self._decoding_layers = []
        for i in range(len(sizes) - 1):
            self._encoding_layers.append(torch.nn.Linear(sizes[i], sizes[i + 1]))
            self._decoding_layers.append(torch.nn.Linear(sizes[i + 1], sizes[i]))
        self._encoding_layers = [torch.nn.Identity()] if len(self._encoding_layers) == 0 else self._encoding_layers
        self._decoding_layers = [torch.nn.Identity()] if len(self._decoding_layers) == 0 else self._decoding_layers
        self._encoding_layers = torch.nn.ModuleList(self._encoding_layers)
        self._decoding_layers = torch.nn.ModuleList(reversed(self._decoding_layers))
        self._loss = torch.nn.MSELoss()
    def forward(self, x):
        y = x.clone()
        for layer in self._encoding_layers:
            x = torch.nn.functional.relu(layer(x))
        bottleneck = x.clone().detach()
        for layer in self._decoding_layers:
            x = torch.nn.functional.relu(layer(x))
        return (x, bottleneck, self._loss(x, y))
    @property
    def input_size(self):
        return self._encoding_layers[0].in_features    
    @property
    def output_size(self):
        return self._decoding_layers[-1].out_features        


# (batch_size :: Int) -> (batch_size x field_representation_size :: Float)
class CategoricalEncoder(torch.nn.Module):
    def __init__(self, spec, embedding_size):
        super(CategoricalEncoder, self).__init__()        
        self._embeddings = torch.nn.Embedding(num_embeddings=len(spec[1]), embedding_dim=embedding_size)
    def forward(self, x):
        return self._embeddings(x)        
    @property
    def input_size(self):
        return 1
    @property
    def output_size(self):
        return self._embeddings.embedding_dim


# (batch_size x entity_representation_size :: Float) -> (batch_size x item_types :: Float)
class CategoricalDecoder(torch.nn.Module):
    def __init__(self, spec, input_size):
        super(CategoricalDecoder, self).__init__()
        output_size = len(spec[1])
        self._layerA = torch.nn.Linear(input_size, output_size)
        self._layerB = torch.nn.Linear(output_size, output_size)
    def forward(self, x):
        x = torch.nn.functional.relu(self._layerA(x))
        x = self._layerB(x)
        x = torch.nn.functional.log_softmax(x, dim=1)
        return x
    @property
    def input_size(self):
        return self._layer.in_features
    @property
    def output_size(self):
        return self._layer.out_features        


# (batch_count :: Float) -> (batch_count :: Float)
class NumericEncoder(torch.nn.Module):
    def __init__(self):
        super(NumericEncoder, self).__init__()
    def forward(self, x):
        return torch.unsqueeze(x, 1)
    @property
    def input_size(self):
        return 1
    @property
    def output_size(self):
        return 1


# (batch_count x entity_representation_size :: Float) -> (batch_count :: Float)    
class NumericDecoder(torch.nn.Module):
    def __init__(self, input_size):
        super(NumericDecoder, self).__init__()
        self._linear = torch.nn.Linear(input_size, 1)
    def forward(self, x):
        return torch.nn.functional.relu(self._linear(x))
    @property
    def input_size(self):
        return self._linear.in_features
    @property
    def output_size(self):
        return self._linear.out_features


# item_sequences -> lengths -> hidden_state
# (batch_count x max_length :: Int) -> (batch_count :: Int) -> (batch_count x entity_representation_size :: Float)
class SequenceEncoder(torch.nn.Module):
    def __init__(self, spec, embedding_size, hidden_size, rnn_type=torch.nn.GRU):
        super(SequenceEncoder, self).__init__()
        self._embeddings = torch.nn.Embedding(num_embeddings=len(spec[1]), embedding_dim=embedding_size)
        self._rnn = rnn_type(embedding_size, hidden_size, batch_first=True)
    def forward(self, x, l):
        embs = self._embeddings(x)
        pk = torch.nn.utils.rnn.pack_padded_sequence(embs, l, batch_first=True, enforce_sorted=False)
        output, h = self._rnn(pk)
        h = h.squeeze(0)
        return h
    @property
    def input_size(self):
        return self._rnn.input_size
    @property
    def output_size(self):
        return self._rnn.hidden_size


# representations -> item_distributions
# (batch_count x entity_representation_size :: Float) -> (batch_count x max_length x item_types :: Float)
class SequenceDecoder(torch.nn.Module):
    def __init__(self, spec, input_size, hidden_size, rnn_type=torch.nn.GRU):
        super(SequenceDecoder, self).__init__()
        self._max_length = spec[2]
        self._rnn = rnn_type(input_size, hidden_size, batch_first=True)
        self._classifier = torch.nn.Linear(hidden_size, len(spec[1]))
    def forward(self, x):
        x = torch.stack([x for i in range(self._max_length)], dim=1)
        output, _ = self._rnn(x)
        outs = []
        for i in range(self._max_length):
            outs.append(torch.nn.functional.log_softmax(self._classifier(output[:, i, :]), dim=1))
        return torch.stack(outs, 1)
    @property
    def input_size(self):
        return self._rnn.input_size
    @property
    def output_size(self):
        return self._rnn.hidden_size


# representations -> summary
# (bottleneck_size x relation_count) -> (bottleneck_size)
class RNNSummarizer(torch.nn.Module):
    def __init__(self, input_size, rnn_type=torch.nn.GRU):
        super(RNNSummarizer, self).__init__()
        self._rnn = rnn_type(input_size, input_size, batch_first=True)
    def forward(self, representations):
        out, h = self._rnn(representations)
        return h


class GraphAutoencoder(torch.nn.Module):
    def __init__(self, data, depth, autoencoder_shapes, embedding_size, hidden_size, mask, field_dropout, hidden_dropout, ae_loss=False, summarizers=RNNSummarizer):
        super(GraphAutoencoder, self).__init__()
        self._data = data
        self._ae_loss = ae_loss
        self._bottleneck_size = 0 if len(autoencoder_shapes) == 0 else autoencoder_shapes[-1]
        self._depth = depth
        self._autoencoder_shapes = autoencoder_shapes
        self._field_order = {}
        self._mask = mask
        self._field_dropout = field_dropout
        self._hidden_dropout = hidden_dropout
        self._embedding_size = embedding_size
        self._hidden_size = hidden_size
        
        # an encoder for each field
        self._field_encoders = {}
        self._entity_rep_sizes = {}
        for entity_type, fields in self._data._entity_fields.items():
            self._field_encoders[entity_type] = {}
            self._field_order[entity_type] = []
            for field_name in [f for f in fields if not f.startswith("*")]:
                t, spec = self._data._fields[field_name]
                self._field_order[entity_type].append(field_name)
                self._field_encoders[entity_type][field_name] = (NumericEncoder() if t == "numeric" else CategoricalEncoder(spec, self._embedding_size) if t == "categorical" else SequenceEncoder(spec, self._embedding_size, self._hidden_size))
            self._entity_rep_sizes[entity_type] = sum([x.output_size for x in self._field_encoders[entity_type].values()])
            self._field_encoders[entity_type] = torch.nn.ModuleDict(self._field_encoders[entity_type])
        self._field_encoders = torch.nn.ModuleDict(self._field_encoders)

        #an autoencoder for each entity type and depth
        self._entity_order = []
        self._entity_autoencoders = {}        
        for entity_type in self._data._entity_fields.keys():
            self._entity_order.append(entity_type)            
            if self._bottleneck_size == None:
                self._entity_autoencoders[entity_type] = [torch.nn.Identity()]
            else:
                boundary_size = sum([m.output_size for m in self._field_encoders[entity_type].values()])                
                self._entity_autoencoders[entity_type] = [Autoencoder([boundary_size] + self._autoencoder_shapes)]
                for depth in range(self._depth):
                    additional_size = 0
                    for other_type in self._data._edges.get(entity_type, {}).keys():
                        additional_size += self._bottleneck_size
                    self._entity_autoencoders[entity_type].append(Autoencoder([boundary_size + additional_size] + self._autoencoder_shapes))
            self._entity_autoencoders[entity_type] = torch.nn.ModuleList(self._entity_autoencoders[entity_type])
        self._entity_autoencoders = torch.nn.ModuleDict(self._entity_autoencoders)

        # a summarizer for each entity type
        self._summarizers = {}
        for entity_type, fields in self._data._entity_fields.items():
            self._summarizers[entity_type] = summarizers(self._bottleneck_size)
        self._summarizers = torch.nn.ModuleDict(self._summarizers)
            
        # a decoder for each field
        self._field_decoders = {}
        for entity_type, fields in self._data._entity_fields.items():
            boundary_size = self._entity_autoencoders[entity_type][-1].output_size
            self._field_decoders[entity_type] = {}
            for field_name in self._field_order[entity_type]:
                t, spec = self._data._fields[field_name]
                self._field_decoders[entity_type][field_name] = (NumericDecoder(boundary_size) if t == "numeric" else CategoricalDecoder(spec, boundary_size) if t == "categorical" else SequenceDecoder(spec, boundary_size, self._embedding_size))
            self._field_decoders[entity_type] = torch.nn.ModuleDict(self._field_decoders[entity_type])
        self._field_decoders = torch.nn.ModuleDict(self._field_decoders)
        
        self._metrics = {"numeric" : torch.nn.MSELoss(reduction="none"),
                         "sequence" : torch.nn.NLLLoss(reduction="none"),
                         "categorical" : torch.nn.NLLLoss(reduction="none"),
        }

    def reconstruct(self, entities, masks, adjacencies):

        reconstructions = {}
        bottlenecks = {}
        entity_reps = {}
        entity_encodings = {}
        ae_losses = []
        
        # encode
        for entity_type, ti in self._data._fields["*entitytype"][1][0].items():
            entity_mask = entities["*entitytype"] == ti
            entity_mask = entity_mask.squeeze(1)
            if entity_mask.sum().item() == 0:
                continue            
            reconstructions[entity_type] = {}
            bottlenecks[entity_type] = {}
            reps = []
            for field_name in self._field_order[entity_type]:
                if field_name == "*entitytype":
                    continue
                vals = entities[field_name][entity_mask]
                t = self._data._fields[field_name][0]
                if field_name in self._mask or (random.random() < self._field_dropout and self.train()):
                    rep = torch.zeros(size=(vals.shape[0], self._field_encoders[entity_type][field_name].output_size), device=entities["*entitytype"].device)
                elif t == "sequence":
                    lens = [v.index(0) if 0 in v else len(v) for v in vals.tolist()]
                    lens = [1 if v == 0 else v for v in lens]
                    rep = self._field_encoders[entity_type][field_name](vals, lens)
                elif t == "categorical":
                    rep = self._field_encoders[entity_type][field_name](vals.squeeze(1))
                else:
                    rep = self._field_encoders[entity_type][field_name](vals.squeeze(1))
                reps.append(rep)
            entity_reps[entity_type] = reps
            
        # run through autoencoders in graphized manner
        node_reps = torch.zeros(size=(entities["*entitytype"].shape[0], 0), device=entities["*entitytype"].device)
        for depth in range(self._depth + 1):
            for entity_type, ti in self._data._fields["*entitytype"][1][0].items():
                entity_mask = entities["*entitytype"] == ti
                entity_mask = entity_mask.squeeze(1)
                if entity_mask.sum().item() == 0:
                    continue
                # line up the relevant neighboring reps
                other_reps = []
                for other_entity_type in self._entity_order:
                    if other_entity_type in adjacencies.get(entity_type, []):
                        # function from (num_adj_ents x rep_size) -> rep_size
                        summarizer = functools.partial(torch.mean, dim=0) if depth == 0 else self._summarizers[other_entity_type]
                        # adjacent ents (num_cur_ents x batch_size)
                        adj = adjacencies[entity_type][other_entity_type][entity_mask]
                        # (num_cur_ents, bottleneck_size)
                        other_entity_reps = torch.zeros(size=(adj.shape[0], node_reps.shape[1]), device=entities["*entitytype"].device)
                        for i in range(adj.shape[0]):
                            if node_reps.shape[1] == 0 or node_reps[adj[i, :]].shape[0] == 0:
                                continue
                            other_entity_reps[i, :] = summarizer(node_reps[adj[i, :]].unsqueeze(0))
                        other_reps.append(other_entity_reps)
                rep = torch.cat(entity_reps[entity_type] + other_reps, 1)
                enc, bottleneck, loss = self._entity_autoencoders[entity_type][depth](rep)
                ae_losses.append(loss)
                entity_encodings[entity_type] = enc
                bottlenecks[entity_type] = bottleneck.detach()
            node_reps = torch.zeros(size=(entities["*entitytype"].shape[0], bottleneck.shape[1]), device=entities["*entitytype"].device)
            for entity_type, ti in self._data._fields["*entitytype"][1][0].items():
                entity_mask = entities["*entitytype"] == ti
                entity_mask = entity_mask.squeeze(1)
                if entity_mask.sum().item() == 0:
                    continue
                node_reps[entity_mask, :] = bottlenecks[entity_type]
                
        # decode
        for entity_type, ti in self._data._fields["*entitytype"][1][0].items():
            entity_mask = entities["*entitytype"] == ti
            entity_mask = entity_mask.squeeze(1)
            if entity_mask.sum().item() == 0:
                continue            
            for field_name in self._field_order[entity_type]:

                out = self._field_decoders[entity_type][field_name](entity_encodings[entity_type])
                targets = entities[field_name][entity_mask]
                t = self._data._fields[field_name][0]
                if targets.shape[0] > 0:
                    if t in ["categorical"]:
                        rout = out
                    elif t in ["sequence"]:
                        rout = []
                        for i in range(out.shape[1]):
                            rout.append(out[:, i, :])
                        rout = torch.stack(rout, 1)
                    else:
                        rout = out #out.squeeze()
                reconstructions[entity_type][field_name] = rout
        return (reconstructions, bottlenecks, ae_losses)
        
    def forward(self, entities, masks, adjacencies):
        losses = []
        originals = {}
        one_field = list(entities.values())[0]
        reconstructions, bottlenecks, ae_losses = self.reconstruct(entities, masks, adjacencies)
        losses_by_architecture = {"autoencoder" : [a.detach().sum().item() for a in ae_losses], "numeric" : [], "categorical" : [], "sequential" : []}
        losses_by_field = {}
        for entity_type, ti in self._data._fields["*entitytype"][1][0].items():
            entity_mask = entities["*entitytype"] == ti
            entity_mask = entity_mask.squeeze(1)
            if entity_mask.sum().item() == 0:
                continue
            recons = reconstructions[entity_type]
            originals[entity_type] = originals.get(entity_type, {})
            for field_name in self._field_order[entity_type]:
                losses_by_field[field_name] = losses_by_field.get(field_name, [])
                field_mask = torch.masked_select(masks[field_name], entity_mask)
                out = recons[field_name]                
                targets = entities[field_name][entity_mask]
                originals[entity_type][field_name] = targets
                t = self._data._fields[field_name][0]
                if targets.shape[0] > 0:
                    if t in ["categorical"]:
                        metric = self._metrics[t]
                        loss = metric(out, targets.squeeze(1))
                        recons[field_name] = out.argmax(1) #.unsqueeze(0)
                        #losses_by_architecture["categorical"].append(loss.detach().sum().item())
                    elif t in ["numeric"]:
                        metric = self._metrics[t]
                        loss = metric(out.squeeze(), targets.squeeze())
                        #losses_by_architecture["numeric"].append(loss.detach().sum().item())
                    elif t in ["sequence"]:
                        metric = self._metrics[t]
                        slosses = []
                        for i in range(out.shape[1]):
                            loss = metric(out[:, i, :], targets[:, i])
                            slosses.append(loss)
                        loss = sum(slosses)
                        #losses_by_architecture["sequential"].append(loss.detach().sum().item())
                        recons[field_name] = out.argmax(2)
                    masked_loss = torch.masked_select(loss, field_mask)
                    losses_by_field[field_name].append(masked_loss.detach().mean().item())
                    losses.append(masked_loss) #torch.masked_select(loss, field_mask))

        #ets = set(list(self._field_order.keys()) + list(reconstructions.keys()))
        #fs = set(sum([list(x) for x in list(self._field_order.values()) + list(reconstructions.values())], []))
        #for et, fs in self._field_order.items():
            #if et not in reconstructions:
            #    reconstructions[et] = {k : torch.zeros(size=()) for k in fs}
            #for f in fs:
            #    if f not in reconstructions[et]:
            #        raise Exception("Reconstructions missing field '{}' for entity type '{}'".format(f, et))

        return sum([torch.sum(x) for x in losses + (ae_losses if self._ae_loss else [])]), reconstructions, originals, losses_by_field, bottlenecks


# Recursively initialize model weights
def init_weights(m):
    if type(m) == torch.nn.Linear or type(m) == torch.nn.Conv1d:
        torch.nn.init.xavier_uniform_(m.weight)
        m.bias.data.fill_(0.01)
