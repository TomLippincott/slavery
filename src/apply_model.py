import gzip
import pickle
import logging
import argparse
import torch
from data import Dataset
from models import GraphAutoencoder
import pandas


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--data", dest="data", help="Input data file")
    parser.add_argument("--model", dest="model", help="Model file")
    parser.add_argument("--gpu", dest="gpu", default=False, action="store_true", help="Use GPU")
    parser.add_argument("--test", dest="test", help="Test split components file")
    parser.add_argument("--value_dropout", dest="value_dropout", default=0.3, type=float, help="Dropout probability for values")
    parser.add_argument("--edge_dropout", dest="edge_dropout", default=0.3, type=float, help="Dropout probability for edges")
    parser.add_argument("--log_level", dest="log_level", default="INFO", choices=["ERROR", "WARNING", "INFO", "DEBUG"], help="Logging level")
    parser.add_argument("--output", dest="output", help="Output file")
    args = parser.parse_args()

    logging.basicConfig(level=getattr(logging, args.log_level))

    with gzip.open(args.model, "rb") as ifd:
        state, data, margs = torch.load(ifd)
    with gzip.open(args.test, "rb") as ifd:
        test_components = pickle.load(ifd)
    with gzip.open(args.data, "rb") as ifd:
        data = pickle.load(ifd)        

    model = GraphAutoencoder(data, margs.depth, margs.autoencoder_shapes, margs.embedding_size, margs.hidden_size, margs.mask, margs.field_dropout, margs.hidden_dropout, margs.autoencoder)
    if args.gpu:
        model.cuda()
        logging.info("CUDA memory allocated/cached: %.3fg/%.3fg", 
                     torch.cuda.memory_allocated() / 1000000000, torch.cuda.memory_cached() / 1000000000)
    model.train(False)

    logging.debug("Model: %s", model)

    outputs = {k : None for k in ["original", "simple"]} #, "mask_values", "mask_fields", "mask_edges", "swap_values", "inject_edges"]}
    #outputs["adjacencies"] = []
    
    def cat(old, new):
        total = 0
        if old == None:
            retval = {k : v.cpu().detach() for k, v in new.items() if len(v.shape) > 0}
        else:
            old_len = max([x.shape[0] if len(x.shape) > 0 else 0 for x in list(old.values())])
            new_len = max([x.shape[0] if len(x.shape) > 0 else 0 for x in list(new.values())])
            total += new_len
            #for f in list(new.keys()):
            #    if len(new[f].shape) == 0:
            #        del new[f]
            
            not_in_new = [f for f in old.keys() if f not in new]
            not_in_old = [f for f in new.keys() if f not in old]
            in_both = [f for f in new.keys() if f in old]
            retval = old
            
            for f in not_in_old:
                retval[f] = torch.zeros(size=(old_len,) + new[f].shape[1:], dtype=new[f].dtype)
                #if len(new[f].shape) == 0:
                #    del new[f]
            for f in not_in_old + in_both:
                if f not in new:
                    continue
                #print(f)
                #print(retval[f].shape)
                #print(new[f].shape)
                #print()
                retval[f] = torch.cat([retval[f].cpu().detach(), new[f].cpu().detach()], dim=0)
            for f in not_in_new:
                retval[f] = torch.cat([retval[f], torch.zeros(size=(new_len,) + old[f].shape[1:], dtype=old[f].dtype)], dim=0)
        return retval
    
    
    for component_id in test_components:        
        entities, masks, adjacencies = data.batch(data._components[component_id])
        entities = {k : torch.tensor(v) for k, v in entities.items()}
        masks = {k : torch.tensor(v).type(torch.ByteTensor) for k, v in masks.items()}
        adjacencies = {source_type : {dest_type : torch.tensor(v) for dest_type, v in dests.items()} for source_type, dests in adjacencies.items()}

        if args.gpu:
            entities = {k : v.cuda() for k, v in entities.items()}
            masks = {k : v.cuda() for k, v in masks.items()}
            adjacencies = {source_type : {target_type : m.cuda() for target_type, m in targets.items()} for source_type, targets in adjacencies.items()}


        
        #outputs["adjacencies"].append(adjacencies)
        #adjacencies)
        # masking values/fields/edges (reconstruction)

        # simple
        _, reconstruction, original, _, _ = model(entities, masks, adjacencies)

        # print("Length: {}".format(entities["*entitytype"].shape[0]))
        
        # print("Numeric:")
        # print(original["a"]["a_numeric"].shape)
        # print(reconstruction["a"]["a_numeric"].shape)

        # print("Categorical:")        
        # print(original["a"]["a_categorical"].shape)
        # print(reconstruction["a"]["a_categorical"].shape)

        # print("Sequential:")        
        # print(original["a"]["a_sequential"].shape)
        # print(reconstruction["a"]["a_sequential"].shape)


        outputs["original"] = cat(outputs.get("original"), dict(sum([list(fields.items()) for _, fields in original.items()], [])))
        #original) #.append((original, adjacencies))
        outputs["simple"] = cat(outputs.get("simple"), dict(sum([list(fields.items()) for _, fields in reconstruction.items()], [])))
        #reconstruction)
        #outputs["original"].append((original, adjacencies))
        #outputs["simple"].append(reconstruction)
        
        # masked values
        #_, reconstruction, _, _, _ = model(entities, masks, adjacencies)
        #mask = None
        #outputs["mask_values"].append((reconstruction, mask))

        # masked edges (will always be strictly less than normal)
        #masked_adjacencies = {}
        #for source, targets in adjacencies.items():
        #    for target, matrix in targets.items():
        #        print(adjacencies.keys())
        #_, _, _, _, bottlenecks = model(entities, masks, adjacencies)
        #mask = None
        #outputs["mask_edges"].append((bottlenecks, mask))
        
        # masked fields
        # reconstructions = []
        # for masked_field_name in [f for f in entities.keys() if f != "*entitytype"]:
        #     continue
        #     entity_type = masked_field_name.split("_")[0]
        #     m = {k : v.clone() for k, v in masks.items()}
        #     e = {k : v.clone() for k, v in entities.items()}
        #     m[masked_field_name] = torch.zeros_like(m[masked_field_name])
        #     e[masked_field_name] = torch.zeros_like(e[masked_field_name])            
        #     _, reconstruction, _, _, _ = model(e, m, adjacencies)
        #     if entity_type in reconstruction:
        #         reconstructions.append((reconstruction[entity_type][masked_field_name].detach(), masked_field_name))
        #outputs["mask_fields"].append(reconstructions)

        # spurious values/edges via swapping/injecting (anomaly detection) (should edge injection be done earlier?)

    #for k, v in outputs.items():
    #    print(k, v.keys())
    with gzip.open(args.output, "wb") as ofd:
        pickle.dump((outputs, margs, model._data._fields), ofd)
