import argparse
import pickle
import gzip
import sys
import logging
import torch
from models import GraphAutoencoder, init_weights
from data import read_entries, unpack_entries, build_auto_spec, compile_data, get_components, batchify


def human(data_spec, entity_type, field_name, value):
    field_type = data_spec[entity_type][field_name][0]
    if field_type == "numeric":
        return value.squeeze().item()
    elif field_type == "categorical":
        lu = {v : k for k, v in data_spec[entity_type][field_name][1].items()}
        return lu[value.item()]
    elif field_type == "sequence":
        lu = {v : k for k, v in data_spec[entity_type][field_name][1].items()}
        value = [v.item() for v in value]
        return "".join([lu[v] for v in value if v != 1])
    else:
        raise Exception()


if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument("-i", "--input", dest="input", help="Input file")
    parser.add_argument("-m", "--model", dest="model", help="Model file")
    parser.add_argument("-o", "--output", dest="output", help="Output file")
    parser.add_argument("--batch_size", dest="batch_size", type=int, default=256, help="Batch size")
    parser.add_argument("--ignore_json_fields", dest="ignore_json_fields", default=[], nargs="+", help="Fields in JSON input to ignore")
    parser.add_argument("--ignore_entity_types", dest="ignore_entity_types", default=[], nargs="+", help="Entity types to ignore")
    parser.add_argument("--source_entity_type", dest="source_entity_type", default="source", help="Entity type of the entry source information")
    parser.add_argument("--merge_equal", dest="merge_equal", default=False, action="store_true",
                        help="When loading entities, merge those with identical field values (otherwise use the source value to keep them distinct)")
    parser.add_argument("--split_proportions", dest="split_proportions", default=[.8, .1, .1], nargs=3, type=float, help="Train/dev/test proportions")
    parser.add_argument("--line_count", dest="line_count", type=int, default=None, help="Only read first L lines")
    parser.add_argument("--entity_types", dest="entity_types", default=None, nargs="*",
                        help="List of entity types to extract based on field prefix, otherwise treat each JSON object as a single entity")
    parser.add_argument("--numeric_fields", dest="numeric_fields", default=None, nargs="+", help="List of numeric fields")
    parser.add_argument("--categorical_fields", dest="categorical_fields", default=None, nargs="+", help="List of categorical fields")
    parser.add_argument("--sequence_fields", dest="sequence_fields", default=None, nargs="+", help="List of sequence fields")
    parser.add_argument("--source_fields", dest="source_fields", default=[], nargs="*", help="Source fields")

    parser.add_argument("--filter_entities", dest="filter_entities", default=None, type=int, help="Ignore entities occurring more than N times")
    parser.add_argument("--max_categorical", dest="max_categorical", default=0, type=int, help="Maximum unique values to consider a text field 'categorical'")
    args = parser.parse_args()

    logging.basicConfig(level=logging.INFO)

    with gzip.open(args.model, "rb") as ifd:
        state, entity_type_lookup, data_spec, gcn_depth, autoencoder_shapes = torch.load(ifd, map_location="cpu")

    entries, field_values = read_entries(args.input, args.ignore_json_fields, args.source_entity_type, args.line_count)
    entity_to_id, id_to_sources, edges, _ = unpack_entries(entries, args.ignore_entity_types, args.source_entity_type, args.merge_equal)

    components = get_components(len(entity_to_id), edges)
    train, dev, test = [batchify(d, args.batch_size) for d in compile_data(entity_to_id, data_spec, components, edges, args.split_proportions, entity_type_lookup, field_values)]

    model = GraphAutoencoder(data_spec, entity_type_lookup, gcn_depth, autoencoder_shapes)
    model.load_state_dict(state)

    all_entities = []

    for entities, adj in test:
        for entity_type, fields in model.reconstruct(entities, adj).items():
            entities = {}
            for i in range(len(list(fields.values())[0])):
                entities[i] = {}
                for field_name, values in [x for x in fields.items() if x[0] != "_type"]:
                    entities[i][field_name] = (human(data_spec, entity_type, field_name, values[0][i]), 
                                               human(data_spec, entity_type, field_name, values[1][i]))

            for entity in entities.values():
                all_entities.append((entity_type, entity))
    
    for entity_type, entity in all_entities:
        print(entity_type)
        for field_name, (original, reconstruction) in entity.items():
            print("\t{}({}): {} --> {}".format(field_name, data_spec[entity_type][field_name][0][0], original, reconstruction))
