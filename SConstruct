import os
import os.path
import logging
import random
import subprocess
import shlex
import gzip
import re
import functools
import time
import imp
import sys
import json
from hashlib import md5
from steamroller.scons import GridBuilder as Builder


# workaround needed to fix bug with SCons and the pickle module
del sys.modules['pickle']
sys.modules['pickle'] = imp.load_module('pickle', *imp.find_module('pickle'))
import pickle


vars = Variables("custom.py")
vars.AddVariables(
    ("OUTPUT_WIDTH", "", 100),

    ("RANDOM_COUNT", "", 10000),
    ("TRAIN_PROPORTION", "", 0.8),
    ("DEV_PROPORTION", "", 0.1),
    ("TEST_PROPORTION", "", 0.1),
    
    ("MAX_CATEGORICAL", "", 50),
    ("RANDOM_LINE_COUNT", "", 1000),
    ("MAX_COLLAPSE", "", 0),

    ("TRAINING_CONFIG", "", {}),
    
    ("DATA_SIZES", "", {}),
    ("FOLDS", "", 2),
    BoolVariable("USE_GPU", "", False),
    ("GRID_LABEL", "", "age"),
    ("GRID_RESOURCES", "", ["h_rt=24:0:0"]),

)


def cmd_builder(interpreter, script, args, other_deps=[], other_args=[], emitter=lambda t, s, e : (t, s, e), before=[]):
    action = " ".join([x.strip() for x in [interpreter, script, args]] + ["${{'--{0} ' + str({1}) if {1} else ''}}".format(a.lower(), a) for a in other_args])
    #print(action)
    def emitter(target, source, env):
        [env.Depends(t, s) for t in target for s in other_deps + [script]]       
        return (target, source)
    return Builder(action=before + [action], emitter=emitter)


env = Environment(variables=vars, ENV=os.environ, TARFLAGS="-c -z", TARSUFFIX=".tgz",
                  BUILDERS={"Generate" : cmd_builder("python",
                                                     "src/generate_test_data.py",
                                                     "--output ${TARGETS[0]} --count ${RANDOM_COUNT}"),
                            "Prepare" : cmd_builder("python",
                                                    "src/prepare_data.py",
                                                    "--input ${SOURCES[0]} --output ${TARGETS[0]} --max_categorical ${MAX_CATEGORICAL} --line_count ${RANDOM_LINE_COUNT} ${'--max_collapse ' + str(MAX_COLLAPSE) if MAX_COLLAPSE else ''}",
                                                    other_deps=["src/data.py"]),
                            "SplitData" : cmd_builder("python",
                                                      "src/split_data.py",
                                                      "--input ${SOURCES[0]} --proportions ${TRAIN_PROPORTION} ${DEV_PROPORTION} ${TEST_PROPORTION} --outputs ${TARGETS}",
                                                      other_deps=["src/data.py"]),
                            "Train" : cmd_builder("python",
                                                  "src/train.py",
                                                  "--data ${SOURCES[0]} --train ${SOURCES[1]} --dev ${SOURCES[2]} --model_output ${TARGETS[0]} --trace_output ${TARGETS[1]} ${'--gpu' if USE_GPU else ''} ${'--autoencoder_shapes ' + ' '.join(map(str, AUTOENCODER_SHAPES)) if AUTOENCODER_SHAPES != None else ''} ${'--mask ' + ' '.join(MASK) if MASK else ''} --log_level DEBUG ${'--keep_entity_types ' + ' '.join(KEEP_ENTITIES) if KEEP_ENTITIES else ''} ${'--autoencoder' if AUTOENCODER else ''}",
                                                  other_deps=["src/models.py", "src/data.py"],
                                                  other_args=["DEPTH", "MAX_EPOCHS", "LEARNING_RATE", "RANDOM_SEED", "PATIENCE", "MOMENTUM", "BATCH_SIZE",
                                                              "EMBEDDING_SIZE", "HIDDEN_SIZE", "FIELD_DROPOUT", "HIDDEN_DROPOUT", "EARLY_STOP"],
                                                  before=["module load cuda90/toolkit"]),
                            "ApplyModel" : cmd_builder("python",
                                                       "src/apply_model.py",
                                                       "--model ${SOURCES[0]} --data ${SOURCES[1]} --test ${SOURCES[2]} --output ${TARGETS[0]} ${'--gpu' if USE_GPU else ''}",
                                                       other_deps=["src/data.py", "src/models.py"],
                                                       before=["module load cuda90/toolkit"]),
                            "Evaluate" : cmd_builder("python",
                                                     "src/evaluate.py",
                                                     "--model ${SOURCES[0]} --data ${SOURCES[1]} --test ${SOURCES[2]} --output ${TARGETS[0]}",
                                                     other_deps=["src/data.py", "src/models.py"]),                            
                            "CollateScores" : cmd_builder("python",
                                                          "src/collate_scores.py",
                                                          "${SOURCES} --output ${TARGETS[0]}"),
                            "CollateTraces" : cmd_builder("python",
                                                          "src/collate_traces.py",
                                                          "${SOURCES} --output ${TARGETS[0]}"),                            
                            "TabulateScores" : cmd_builder("python",
                                                           "src/tabulate_scores.py",
                                                           "--input ${SOURCES[0]} --output ${TARGETS[0]}"),
                            "PlotTraces" : cmd_builder("python",
                                                       "src/plot_traces.py",
                                                       "--input ${SOURCES[0]} --output ${TARGETS[0]}"),                            
                  },
                  tools=["default"],
)


# function for width-aware printing of commands
def print_cmd_line(s, target, source, env):
    if len(s) > int(env["OUTPUT_WIDTH"]):
        print(s[:int(float(env["OUTPUT_WIDTH"]) / 2) - 2] + "..." + s[-int(float(env["OUTPUT_WIDTH"]) / 2) + 1:])
    else:
        print(s)


# and the command-printing function
env['PRINT_CMD_LINE_FUNC'] = print_cmd_line


# and how we decide if a dependency is out of date
env.Decider("timestamp-newer")


def run_experiment(env, dataset=None, **args):
    if dataset == None:
        dataset = env.Generate("work/${EXPERIMENT}/dataset.json.gz", [], **args)
    prepped_dataset = env.Prepare("work/${EXPERIMENT}/prepped_dataset.pkl.gz", dataset, **args)

    configs = [[]]
    for arg_name, values in env["TRAINING_CONFIG"].items():
        configs = sum([[config + [(arg_name.upper(), v)] for config in configs] for v in values], [])
    configs = [dict(config) for config in configs]
    outputs = []
    traces = []
    for fold in range(1, env["FOLDS"] + 1):
        args["FOLD"] = fold
        args["RANDOM_SEED"] = fold        
        train_split, dev_split, test_split = env.SplitData(["work/${EXPERIMENT}/${FOLD}/%s.pkl.gz" % (x) for x in ["train", "dev", "test"]],
                                                           prepped_dataset,
                                                           **args)

        for config in configs:
            args["CONFIG_ID"] = md5(str(sorted(list(config.items()))).encode()).hexdigest()
            model, trace = env.Train(["work/${EXPERIMENT}/${FOLD}/model_${CONFIG_ID}.pkl.gz", "work/${EXPERIMENT}/${FOLD}/trace_${CONFIG_ID}.pkl.gz"],
                                     [prepped_dataset, train_split, dev_split],
                                     GRID_RESOURCES=["h_rt=24:0:0", "gpu=1"],
                                     GRID_QUEUE="gpu.q@@1080",
                                     **args,
                                     **config)
            traces.append(trace)
            outputs.append(env.ApplyModel(["work/${EXPERIMENT}/${FOLD}/test_output_${CONFIG_ID}.pkl.gz"],
                                          [model, prepped_dataset, test_split],
                                          GRID_RESOURCES=["h_rt=24:0:0", "gpu=1"],
                                          GRID_QUEUE="gpu.q@@1080",
                                          **args,
                                          **config))

    collated_scores = env.CollateScores("work/${EXPERIMENT}/results.txt.gz", sorted(outputs), **args, **config)
    collated_traces = env.CollateTraces("work/${EXPERIMENT}/traces.txt.gz", sorted(traces), **args, **config)    
    score_table = env.TabulateScores("work/${EXPERIMENT}/score_table.txt.gz", collated_scores, **args, **config)
    learning_plot = env.PlotTraces("work/${EXPERIMENT}/loss_trace.png", collated_traces, **args, **config)


env.AddMethod(run_experiment, "RunExperiment")


#
# Experiments
#

# env.RunExperiment(EXPERIMENT="random", MAX_COLLAPSE=0, MAX_CATEGORICAL=20)
# env.RunExperiment(EXPERIMENT="random_collapsed", MAX_COLLAPSE=1000, MAX_CATEGORICAL=20)
# env.RunExperiment(EXPERIMENT="random_masked", MAX_COLLAPSE=0, MAX_CATEGORICAL=20,
#                   MASK=["{}_{}".format(t, f) for t in ["a", "b"] for f in ["categorical",
#                                                                            "numeric",
#                                                                            "sequential"]])
# env.RunExperiment(EXPERIMENT="random_collapsed_masked", MAX_COLLAPSE=1000, MAX_CATEGORICAL=20,
#                   MASK=["{}_{}".format(t, f) for t in ["a", "b"] for f in ["categorical",
#                                                                            "numeric",
#                                                                            "sequential"]])

slave_data = env.File("data/slavery.json.gz")
for size, (line_count, max_categorical) in env["DATA_SIZES"].items():
    env.RunExperiment(slave_data, EXPERIMENT="slavery_{}".format(size), MAX_COLLAPSE=0, RANDOM_LINE_COUNT=line_count, MAX_CATEGORICAL=max_categorical)
    env.RunExperiment(slave_data, EXPERIMENT="slavery_{}_collapsed".format(size), MAX_COLLAPSE=100, RANDOM_LINE_COUNT=line_count, MAX_CATEGORICAL=max_categorical)


